# TermTraGS (Term Translation Gold Standard) 
TermTraGS is an English-Italian parallel corpus in the IT domain where domain-specific terms in the test set have been automatically marked 
by means of a terminology lexicon extracted from [Bitter corpus](https://hlt-mt.fbk.eu/technologies/bittercorpus "Bitter corpus download page").
The original Bitter corpus contains parallel documents extracted from the GNOME and the KDE4 domain in which domain-specific terms have been manually marked and aligned.
Unfortunately, the Bitter corpus contains only the annotated test data without any in-domain training set, 
while in the adaptive neural machine translation (NMT) we need both the training and test data.

So, we created the TermTraGS corpus which contains sentence-level aligned training and test sets.
The creation of this corpus is described in the following paper, in which it is used for evaluating static and adaptive (corpus- and instance-based) neural MT systems:

M. Amin Farajian, Nicola Bertoldi, Matteo Negri, Marco Turchi, Marcello Federico "Evaluation of Terminology Translation in Instance-Based Neural MT Adaptation". 
In Proceedings of EAMT 2018.

If you use the corpus, please cite the above paper.